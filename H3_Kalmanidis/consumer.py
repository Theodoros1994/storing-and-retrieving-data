# -*- coding: utf-8 -*-
"""
Created on Tue Feb 25 20:59:16 2020

@author: 30694
"""
#import json and kafka libraries
from json import loads
from kafka import KafkaConsumer


#decode the input using the utf-8 unicode transformation format
dezer=lambda x:loads(x.decode('utf-8'))


#initialize the consumer by passing some important variables like, that it will live on the localhost:9092, auto commit interval is true which means that we allow
# the concumer to read again the same message if the offset is not commited within one second
consumer=KafkaConsumer('numtest',bootstrap_servers=['localhost:9092'],auto_offset_reset='earliest',enable_auto_commit=True,group_id='my-group',value_deserializer=dezer)



#declate the addition variable
suma=0

#parse through each message
for message in consumer:
    # and get it's value
    message=message.value
    # then pass it and add it to our addition variable and display it to the user
    suma+=message['number']
    print("Up-to-now SUM=%d" %suma)










