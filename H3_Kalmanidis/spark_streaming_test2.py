# -*- coding: utf-8 -*-
"""
Created on Fri Feb 28 02:33:39 2020

@author: 30694
"""

import os
os.environ['PYSPARK_SUBMIT_ARGS'] ='--packages org.apache.spark:spark-streaming-kafka-0-8-assembly_2.11:2.3.2 pyspark-shell'
from pyspark import SparkContext
# Spark Streaming
from pyspark.streaming import StreamingContext
# Kafka
from pyspark.streaming.kafka import KafkaUtils
# json parsing
import json











sc = SparkContext(appName="PythonSparkStreamingKafka_RM_01")
sc.setLogLevel("WARN")
ssc = StreamingContext(sc, 60) # batch duration = 60 seconds
# connect to kafka
kafkaStream = KafkaUtils.createStream(ssc, 'localhost:2181', 'spark-streaming', {'twitter':1})


#load json files to the parsed variable
parsed = kafkaStream.map(lambda v: json.loads(v[1]))


#use the count function on the mapped messages that are passed to the parsed variable contained in each batch
parsed.count().map(lambda x: 'Tweets in this batch: %s' % x).pprint()






# authors_dstream = parsed.map(lambda tweet: tweet['user']['screen_name'])


# author_counts = authors_dstream.countByValue()
# author_counts.pprint()




# author_counts_sorted_dstream = author_counts.transform(lambda foo: foo.sortBy(lambda x: -x[1]))
# author_counts_sorted_dstream.pprint()


# top_five_authors = author_counts_sorted_dstream.transform(lambda rdd: sc.parallelize(rdd.take(5)))
# top_five_authors.pprint()



# filtered_authors = author_counts.filter(lambda x: x[1]>1 or x[0].lower().startswith('rm'))
# filtered_authors.transform(lambda rdd: rdd.sortBy(lambda x: -x[1])).pprint()


#within the 'msg' attribute of the json file, split every word with space and count it's value
#once that has been completed use the transformation function to sort and present it's word along with the calculated count value.
parsed.flatMap(lambda tweet: tweet['msg'].split(" ")).countByValue().transform(lambda rdd: rdd.sortBy(lambda x: -x[1])).pprint()


ssc.start()
ssc.awaitTermination()













