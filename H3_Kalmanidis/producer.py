# -*- coding: utf-8 -*-
"""
Created on Tue Feb 25 20:52:23 2020

@author: 30694
"""
#import time,json and kafka libraries
from time import sleep
from json import dumps
from kafka import KafkaProducer

#decode the input using the utf-8 unicode transformation format
serzer=lambda x:dumps(x).encode('utf-8')

#intialize the KafkaProducer variable by passing the right configuration parameters
producer=KafkaProducer(bootstrap_servers=['localhost:9092'],value_serializer=serzer)

if __name__ == "__main__":
  #within the main function we generate 1000 numerical numbers and we sent them to our Kafka Pipeline every 5 seconds
    for e in range(1000):
        data={'number': e}
        producer.send('numtest',value=data)
        sleep(5)
