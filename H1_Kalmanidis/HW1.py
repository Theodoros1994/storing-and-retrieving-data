# -*- coding: utf-8 -*-
"""
Created on Thu Jan 30 19:08:50 2020

@author: 30694
"""

#Impoty the necessary libraries
from mrjob.job import MRJob
from collections import Counter
import concurrent.futures as cf
import sys
from datetime import datetime
import time
#=============================Ex1 . a====================
# from mrjob.job import MRJob

class MRWordMax(MRJob):
      def mapper(self,_,line):
# For each line on our text file let's split all our numbers
          for number in line.split():
# For each number that is splitted let's get it's value in integer form and pass it to our yield func
              yield (None,int(number))
# Pass all our available integer numbers from our mapper to our reducer          
      def reducer(self,_,numbers):
# and yield only the maximum number of our total list of int numbers
              yield("Max value is", max(numbers))


if __name__=='__main__':
# Run the MapReduce Class
  MRWordMax.run()     
 
 
 
#===================================== 


#=============================Ex1 . b====================

# class MRWordUnique(MRJob):
#       def mapper(self,_,line):

# #For each number that is splitted let's get it's value in integer form and pass it to our yield func          
#           for number in line.split():
# #Again we pass the integer form of our numbers included as we did before    
#             yield (None,int(number))
          
#       def reducer(self,_,numbers):
# # For this part we are going to use the List(set()) combination which returns a list of unique elements within a list          
# #A set is an unordered collection data type with no duplicate elements. Sets are iterable and mutable. The elements appear in an arbitrary order when sets are iterated.
# #https://www.hackerearth.com/practice/python/working-with-data/set/tutorial/          
#               yield("The unique values are",list(set(numbers)))


if __name__=='__main__':
    MRWordMax.run()       
#For the last part we are going to    compare the time for the different .txt files
# so we declare a variable for our start timestamp of our program's execution which will be the moment we will call the
#MRJOB class from the command prompt    
    start_time=datetime.now()
#Then we call the first function which is the MRWordMax class.   
    MRWordMax.run()
#Once the calculations within the class have been completed we track the end time that this event occured.   
    end_time=datetime.now()
#We calculate and we print the elapsed time for this particular file we used as an input   
    elapsed_time=end_time-start_time
    print(elapsed_time)
                # MRWordCount.run()
                
        #         print("result=%d in " %(e1)) 
        # # r=sum(results)
 
     
 
 
 
#===================================== 
















# def mapper(key, value):
#     print(key,max(value))

# def reducer(key, list_of_values):
#     print(max(list_of_values))
 
 
    
  #   yield None, int(number)

  # # Discard key, because it is None
  # # After sort and group, the output is only one key-value pair (None, <all_numbers>)
  # def reducer(self, _, numbers):
  #   yield "Max value is", max(numbers)    
    
    
#  lst = []
# def mapper(key, value):
#     lst.append(max(value))
#     print(key, max(lst))

# def reducer(key, list_of_values):
#     print(max(list_of_values))
 
 
 
 
# from mrjob.job import MRJob

# class MRWordCount(MRJob):
#      def mapper(self,_,line):
#          for word in line.split():
#              yield (word,1)
#      def reducer(self,word,counts):
#           yield(word,max(max(counts)))


# if __name__=='__main__':
#  MRWordCount.run()     
 