# -*- coding: utf-8 -*-
"""
Created on Mon Feb 10 09:22:48 2020

@author: 30694
"""

import sys
from pyspark import SparkContext

if __name__=="__main__":
    sc=SparkContext("local[*]", "WordCount")
    lines = sc.textFile(sys.argv[1])

    lines2=lines.flatMap(lambda s: s.split(" ")) \
    .map(lambda word: (word, 1)) \
    .reduceByKey(lambda x, y: x + y) \
   .map(lambda x: (x[1],x[0])) \
   .sortByKey(False)
    print("Top 10 words")
    print (lines2.take(10))
    
    
    
    # lines2=lines2.map(lambda x:(x[1],x[0]).sortByKey(False).take(10))
    # lines2.saveAsTextFile("C:\spark-2.4.4-bin-hadoop2.7\spark-2.4.4-bin-hadoop2.7\outputttt") 
