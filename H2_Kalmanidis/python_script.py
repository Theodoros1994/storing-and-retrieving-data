# -*- coding: utf-8 -*-
"""
Created on Mon Feb 10 09:57:39 2020

@author: 30694
"""
# FROM io.STRINGIO
import sys
from pyspark import SparkContext
sc = SparkContext('local[*]', 'PageRank')
NUM_ITERATIONS = 5
def cl2n(s):
  r = list(s)
  last = r.pop()
  r.append(float(last))
  return r

urls = sc.textFile('C:\\Users\\30694\\url_pages.txt').map(lambda s: s.split(' '))
ranks = sc.textFile('C:\\Users\\30694\\url_ranks.txt').map(lambda s: s.split(' ')).map(cl2n)

def get_contribs(pair):
  [url, [links, rank]] = pair
  return [(dest, rank / len(links)) for dest in links]

if __name__=='__main__':
  for i in range(NUM_ITERATIONS):
    print ("iteration %s" % i)
    contribs0 = urls.join(ranks)
    print ("contribs0=%s" % contribs0.collect())
    contribs = contribs0.flatMap(get_contribs)
    print ("contribs=%s" % contribs.collect())
    ranks = contribs.reduceByKey(lambda x, y: x+y) \
                    .mapValues(lambda x: 0.15+0.85*x)
  for (link, rank) in ranks.collect():
    print ("%s has rank: %s." % (link, rank))
  sc.stop()
