# -*- coding: utf-8 -*-
"""
Created on Wed Mar 11 00:47:17 2020

@author: 30694
"""
#Import all the useful libraries
import json
import pymongo
from bson.son import SON
import pprint


#Initialize our Mongodb Client
client = pymongo.MongoClient("mongodb://localhost:27017/")

# Create a database called homeworkdatabase
mydb = client["homeworkdatabase"]
 
#Create a Collection called users
mycol = mydb["users"]
#Clean Data that may prexist
mycol.drop()
mycol = mydb["users"]
# In MongoDB, a collection is not created until it gets content!

collist = mydb.list_collection_names()
if "users" in collist:
 print("The collection exists.")
 
 # Create 2 empty lists for storage of interests and the whole json document row
string3=[]
mylist=[]
#Create a 1000step loop
for i in range(1,1001):
    # assign loop number to the _id of each document
  idnumber=i
  # construct the fields by adding the counter's number in front of each word first,last
  string1='first'+str(i)
  string2='last'+str(i)
  # truck the history of the generated interests for instance interest1, interest2,
  string3_old='interest'+str(i) 
  string3.append(string3_old)
  # Create the Json Object for each row, notice for the Interests we add the values generated from previous rows also
  myJsonObject ={"_id":idnumber,"FirstName":string1, "LastName":string2,  "Interests":string3[:i]} 
  mylist.append(myJsonObject )


# Print our 1000 documents list
print(mylist)


# Insert documents to database
x = mycol.insert_many(mylist)
# See the inserted documents form
print(x.inserted_ids)






# Query to see how many times a specific interest appears in the whole collection.
pipeline1=[{"$unwind": "$Interests"},{"$group": {"_id": "$Interests", "count": {"$sum": 1}}},{"$sort": SON([("count", 1), ("_id", 1)])}]



result=mycol.aggregate(pipeline1)
pprint.pprint(list(result))

#==Bonus ==

#Query to count the number of interests for each users that appear in all documents
pipeline2=[{"$unwind": "$Interests"},{"$group": {"_id": "$_id", "count": {"$sum": 1}}},{"$sort": SON([("count", 1), ("_id", 1)])}]

#Pass the query via pipeline to the aggregate method of our collection
result=mycol.aggregate(pipeline2)
#convert to list the json result to display it properly to the console
pprint.pprint(list(result))




#Query to see how many interests (and their names) are contained in a specific document 
pipeline3=[{ "$group": {
        "_id": "$LastName",
        "Interests": { "$push": "$Interests" },
        "total": { "$sum": 1 }
    }},
    { "$unwind": "$Interests" },
    { "$unwind": "$Interests" },
    { "$group": {
        "_id": {
            "LastName": "$_id",
            "hobby": "$Interests"
        },
        "total": { "$first": "$total" },
        "hobbyCount": { "$sum": 1 }
    }},
    { "$group": {
        "_id": "$_id.LastName",
        "total": { "$first": "$total" },
        "Interests": {
            "$push": { "name": "$_id.hobby", "count": "$hobbyCount" }
        }
    }}]




result=mycol.aggregate(pipeline3)


pprint.pprint(list(result))

